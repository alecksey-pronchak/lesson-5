﻿internal class Program
{
    static void Task1 ()
    {
       string str1 = "testTe12ach56test36MeS42kil44test3246ls";
       string str2 = string.Empty;
        foreach (char c in str1) 
        {
            if (!char.IsDigit(c))
                str2 += c;
        }
        str2 = str2.Replace("test", "");
        Console.WriteLine($"This is start string: {str1}");
        Console.WriteLine($"And this is result of working this programm: {str1} ===== {str2}");
    }
    static void Task2 ()
    {
        string str1 = "\"Welcome\"";
        string str2 = "\"to\"";
        string str3 = "\"the\"";
        string str4 = "\"ТMS\"";
        string str5 = "\"lessons\"";
        string str6 = string.Empty;
        string[] arr = { str1, str2, str3, str4, str5, };
        Console.WriteLine($"This string after string.Join: {str6 = string.Join(" ", arr)}");
        str6 = str6.Replace("\"", "");
        Console.WriteLine($"This is string after string.Replace: {str6}");
    }
    static void Task3()
    {
        string str1 = "teamwithsomeofexcersicesabcwanttomakeitbetter";
        string str2 = string.Empty;
        string str3 = string.Empty;
        Console.WriteLine("Index of abc in string: " + str1.LastIndexOf("abc"));
        for (int i = 0; i < str1.LastIndexOf("abc"); i++)
            str2 += str1[i];
        Console.WriteLine("Characters up to abc: " + string.Join (" ", str2));
        for (int i = str1.LastIndexOf("abc") + 3; i < str1.Length; i++)
            str3 += str1[i];
        Console.WriteLine("Characters after abc: " + string.Join(" ", str3));
    }
    static void Task4()
    {
        string text = "Плохой день";
        Console.WriteLine("Start string: " + text);
        text = text.Substring(7);
        Console.WriteLine("String after Substring method: " + text);
        string text2 = "Хороший ";
        string text3 = "!!!!!!!!!";
        text = text.Insert(0, text2);
        text = text.Insert(12, text3);
        Console.WriteLine("String after Insert method: " + text);
        for (int i = 0; i < text.Length;  i++)
        {
            if (i == text.Length - 1)
            {
                text = text.Remove(i);
                text = text.Insert(i, "?");
            }
        }
        Console.WriteLine("String after remove last !: " + text);
    }


    public static void Main(string[] args)
    {
        Console.WriteLine("Hello, please, choose one task (from 1 to 5):");
        int.TryParse(Console.ReadLine(), out int val);
        switch (val)
        {
            case 1: 
                Task1();

                break;
            case 2:
                Task2();
                break;
            case 3:
                Task3();
                break;
            case 4:
                Task4();
                break;
            default: Console.WriteLine("Error has occured!");
                break;
        }
    }
}